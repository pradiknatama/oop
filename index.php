<?php 
    require_once ('animal.php');
    require_once ('ape.php');
    require_once('frog.php');

    // release 0
    $sheep = new Animal("shaun");

    echo "Name : ".$sheep->name."<br>";
    echo "Legs : ".$sheep->legs."<br>";
    echo "cold blooded : ".$sheep->cold_blooded."<br><br>";

    // release 1
    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>";
    echo "Legs : ".$kodok->legs."<br>";
    echo "cold blooded : ".$kodok->cold_blooded."<br>";
    $kodok->jump() ;
    echo "<br><br>";

    $sungokong=new Ape("Kera Sakti");
    echo "Name : ".$sungokong->name."<br>";
    echo "Legs : ".$sungokong->legs."<br>";
    echo "cold blooded : ".$sungokong->cold_blooded."<br>";
    $sungokong->yell();
    

?>